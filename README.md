## Cannon-GDX

A LibGDX-based client for [cannon-go](https://gitlab.com/donachys/cannon-go).

to build the project `./gradlew desktop:dist`
an instance of the cannon-go game server will need to be running.


#### Game Description

Inspired by the game gorillas and battleship, you are trying to hit your opponent's cannon. The catch is that you dont know exactly where it is, but it's somewhere within your opponent's castle walls. Modify the rotation, pitch, and power parameters to try and hit it!

![demo](images/cannongdxdemo.gif)