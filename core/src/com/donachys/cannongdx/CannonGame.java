package com.donachys.cannongdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import com.donachys.cannongdx.Screens.GameScreen;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import static com.badlogic.gdx.math.MathUtils.random;

public class CannonGame {
    public static int numPlayers = 2;
    private Stage stage;

    private final int CANNONSPACE_X = 88;
    private final int CANNONSPACE_Y = 50;

    public float width_ratio, height_ratio;

    private GameState lastGameState;
    public float rotation, pitch, power;

    private Image cannon;
    private ArrayList<Image> bases = new ArrayList<Image>();

    private List<List<Point>> pointMisses = (List) new ArrayList<ArrayList<Point>>();
    private LinkedBlockingQueue<Point> playerShotQueue;
    private LinkedBlockingQueue<Point> oppShotQueue;
    private CannonGdx gdxgame;
    private GameScreen gs;

    private Object lock = new Object();

    private AssetManager mgr = new AssetManager();

    public CannonGame(Stage stage, CannonGdx gdxgame, GameScreen gs) {
        for (int i=0; i<numPlayers; i++) {
            pointMisses.add(i, new ArrayList<Point>());
        }

        playerShotQueue = new LinkedBlockingQueue<Point>();
        oppShotQueue = new LinkedBlockingQueue<Point>();

        this.stage = stage;
        this.gdxgame = gdxgame;
        this.gs = gs;

        width_ratio = (float) gdxgame.width/CANNONSPACE_X;
        height_ratio = (float) gdxgame.height/CANNONSPACE_Y;

        rotation = random.nextFloat()*36000 / 100.0f;
        pitch = random.nextFloat()*4000 / 100.0f + 45.0f;
        power = random.nextFloat()*3000 / 100.0f + 5.0f;

        initialize();

    }

    public void handleStreamingPlayResponse(StreamingPlayResponse resp) {
        ShotsFired allShots = resp.getShotsFired();
        List<Shots> shotsList = allShots.getShotsList();
        for (Shots pshots : shotsList) {
            if (pshots.getPlayerNum() == resp.getGameState().getPlayerNum()) {
                playerShotQueue.addAll(pshots.getShotsList());
            }  else {
                oppShotQueue.addAll(pshots.getShotsList());
            }
        }

        synchronized (lock) {
            if ((lastGameState == null ||
                lastGameState.getState() == GameState.State.WAITING) &&
                resp.getGameState().getState() == GameState.State.ACTIVE) {
                addStaticActors();
                float[] rng = rotRange(
                        resp.getGameState().getP0Base(),
                        resp.getGameState().getP1Base(),
                        resp.getGameState().getPlayerNum());
                rotation = random.nextFloat()*9000 / 100.0f + rng[0];
                setCannonRot(rotation);
                Gdx.app.log("info", "rot: " + rotation + " 0: " + rng[0] + " 1: " + rng[1]);
                gs.setRotSliderRange(rng[0], rng[1]);
                gs.addGameUI();
            }
            lastGameState = resp.getGameState();
            updateActors();
        }
    }
    private float[] rotRange(Rect p0Base, Rect p1Base, int pnum) {
        Point p0mid = Point.newBuilder()
                .setX(p0Base.getTopLeft().getX() + p0Base.getBotRight().getX() / 2)
                .setY(p0Base.getTopLeft().getY() + p0Base.getBotRight().getY() / 2)
                .build();
        Point p1mid = Point.newBuilder()
                .setX(p1Base.getTopLeft().getX() + p1Base.getBotRight().getX() / 2)
                .setY(p1Base.getTopLeft().getY() + p1Base.getBotRight().getY() / 2)
                .build();
        Point adjustedTargetPoint;
        switch(pnum) {
            case 0:
                adjustedTargetPoint = Point.newBuilder().setX(p1mid.getX() - p0mid.getX()).setY(p1mid.getY() - p0mid.getY()).build();
                break;
            case 1:
                adjustedTargetPoint = Point.newBuilder().setX(p0mid.getX() - p1mid.getX()).setY(p0mid.getY() - p1mid.getY()).build();
                break;
            default:
                return new float[]{0f, 360f};
        }
        float rads = (float)Math.atan2(adjustedTargetPoint.getY(), adjustedTargetPoint.getX());
        float degs = (float)(rads*180/Math.PI);
        return new float[]{degs-45.0f, degs+45.0f};
    }

    public void updateActors(){
        synchronized (lock) {
            for (int i = 0; i < numPlayers; i++) {
                int offset = 1;
                Point p;
                List<Point> pmisses;
                switch (i) {
                    case 0:
                        p = cannonSpaceToScreenSpace(lastGameState.getP0Base().getTopLeft());
                        pmisses = lastGameState.getP0MissesList();
                        break;
                    case 1:
                        p = cannonSpaceToScreenSpace(lastGameState.getP1Base().getTopLeft());
                        pmisses = lastGameState.getP1MissesList();
                        break;
                    default:
                        continue;
                }
                Point op = cannonSpaceToScreenSpace(Point.newBuilder().setX(offset).setY(offset).build());
                bases.get(i).setPosition(p.getX()-op.getX(), p.getY()-op.getY());
                pointMisses.set(i, pmisses);
            }
        Point p = cannonSpaceToScreenSpace(lastGameState.getCannon());
        cannon.setPosition(p.getX(), p.getY());
        }
    }

    public void initialize() {
        mgr.load("cannon0216x16.png", Texture.class);
        mgr.load("castle01256x256.png", Texture.class);
        mgr.finishLoading();
        Texture cannonTex = mgr.get("cannon0216x16.png", Texture.class);
        Texture baseTex = mgr.get("castle01256x256.png", Texture.class);
        cannon = new Image(cannonTex);
        cannon.setScale(width_ratio/16, height_ratio/16);
        cannon.setOrigin(Align.center);
        cannon.setRotation(rotation);
        int baseTexDim = 256;
        int outerWallDim = 7;
        Image p0base = new Image(baseTex);
        p0base.setScale(width_ratio*outerWallDim/baseTexDim, height_ratio*outerWallDim/baseTexDim);
        bases.add(p0base);

        Image p1base = new Image(baseTex);
        p1base.setScale(width_ratio*outerWallDim/baseTexDim, height_ratio*outerWallDim/baseTexDim);
        bases.add(p1base);
    }

    public void addStaticActors() {
        for (int i=0; i<numPlayers; i++) {
            stage.addActor(bases.get(i));
        }
        stage.addActor(cannon);
    }

    public Point cannonSpaceToScreenSpace(Point p) {
        return Point.newBuilder().setX((int)(width_ratio*p.getX())).setY((int)(height_ratio*p.getY())).build();
    }

    public List<Point> getPointMisses(int index) {
        if (index >= pointMisses.size()) {
            return new ArrayList<Point>();
        }
        return pointMisses.get(index);
    }

    public List<Point> collectPlayerShots() {
        ArrayList<Point> list = new ArrayList<Point>();
        playerShotQueue.drainTo(list);
        return list;
    }

    public List<Point> collectOppShots() {
        ArrayList<Point> list = new ArrayList<Point>();
        oppShotQueue.drainTo(list);
        return list;
    }

    public Point getCannonPoint() {
        return Point.newBuilder().setX((int)cannon.getX()).setY((int)cannon.getY()).build();
    }
    public void setCannonRot(float degrees) {
        cannon.setRotation(degrees);
    }

    public GameState.Outcome getOutcome() {
        if (lastGameState != null) {
            return lastGameState.getOutcome();
        }
        return GameState.Outcome.NONE;
    }

    public GameState.State getState() {
        if (lastGameState != null) {
            return lastGameState.getState();
        }
        return GameState.State.WAITING;
    }

    public float euclDist(Point a, Point b) {
        int ydif = a.getY() - b.getY();
        int xdif = a.getX() - b.getX();
        return (float)Math.sqrt((xdif * xdif) + (ydif * ydif));
    }
}
