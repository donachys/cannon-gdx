package com.donachys.cannongdx.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import com.donachys.cannongdx.CannonGame;
import com.donachys.cannongdx.CannonGdx;
import com.donachys.cannongdx.CannonGoClient;
import com.donachys.cannongdx.GameState;
import com.donachys.cannongdx.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class GameScreen implements Screen {
    private int numPlayers = CannonGame.numPlayers;
    private Stage gameStage;
    private Stage UIStage;
    private CannonGdx game;
    private InputMultiplexer multiplexer;
    private OrthographicCamera camera;

    private CannonGoClient client;
    private CannonGame cg;

    private ArrayList<ArrayList<Image>> misses = new ArrayList<ArrayList<Image>>();

    private Slider zoomSlider;
    private Slider rotSlider;
    private Slider powerSlider;
    private Slider pitchSlider;

    private Label rotSliderLabel;
    private Label powerSliderLabel;
    private Label pitchSliderLabel;

    private TextButton fireButton;
    public static final String ASSETS = "assets/";

    private AssetManager mgr = new AssetManager();
    Texture pshotTex;
    Texture pCraterTex;
    Texture oppshotTex;
    Texture oppCraterTex;

    public GameScreen(CannonGdx incGame, CannonGoClient incClient) {
        for (int i=0; i<numPlayers; i++) {
            misses.add(i, new ArrayList<Image>());
        }

        client = incClient;
        game = incGame;
        camera = new OrthographicCamera();
        gameStage = new Stage(new FitViewport(game.width, game.height, camera));
        UIStage = new Stage(new FitViewport(game.width, game.height));

        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(UIStage);
        multiplexer.addProcessor((gameStage));

        mgr.load("cannonball_orange128x128.png", Texture.class);
        mgr.load("crater0_orange128x128.png", Texture.class);
        mgr.load("cannonball_purp128x128.png", Texture.class);
        mgr.load("crater0_purp128x128.png", Texture.class);
        mgr.load("grassland1680x945.png", Texture.class);
        mgr.finishLoading();
        pshotTex = mgr.get("cannonball_orange128x128.png", Texture.class);
        pCraterTex = mgr.get("crater0_orange128x128.png", Texture.class);
        oppshotTex = mgr.get("cannonball_purp128x128.png", Texture.class);
        oppCraterTex = mgr.get("crater0_purp128x128.png", Texture.class);

        cg = new CannonGame(gameStage, game, this);
        addPrepStage();
    }

    @Override
    public void show() {
        Gdx.app.log("MainScreen","show");
        Gdx.input.setInputProcessor(multiplexer);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.859f, 0.914f, 0.469f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        pShotUpdates();
        oppShotUpdates();
        GameState.Outcome outcome = cg.getOutcome();
        if (cg.getState() == GameState.State.COMPLETE) {
            String outcomeText;
            switch (outcome) {
                case WIN:
                    outcomeText = "You WIN";
                    break;
                case LOSS:
                    outcomeText = "You LOSE";
                    break;
                case CANCELED:
                    outcomeText = "game Canceled, please try again";
                    break;
                default:
                    outcomeText = "Game Over";
            }
            new Thread() {
                public void run(){
                    try {
                        client.shutdown();
                    } catch (InterruptedException e) {
                        Gdx.app.log("err", "[ERROR]: " + e.getMessage());
                    }
                }
            }.start();

            Dialog d = new Dialog("Game over...", CannonGdx.skin).text(outcomeText, new Label("sometext", CannonGdx.skin, "default").getStyle());
            d.setColor(0, 0, 0, 0.95f);
            d.setFillParent(true);
            TextButton restart = new TextButton("Restart", CannonGdx.skin);
            restart.addListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("restart button");
                    game.setScreen(new TitleScreen(game));
                    dispose();
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }
            });
            d.button(restart);
            UIStage.addActor(d);
        }
        UIStage.act();
        gameStage.act();
        gameStage.draw();
        UIStage.draw();

    }

    public void pShotUpdates() {
        List<Point> pshots = cg.collectPlayerShots();
        float velocity = 150f;
        int scaleUp = 4;
        for (Point shot : pshots) {
            final Image shotActor = new Image(pshotTex);
            Point p = cg.cannonSpaceToScreenSpace(shot);
            shotActor.setWidth(cg.width_ratio);
            shotActor.setHeight(cg.height_ratio);
            shotActor.setPosition(cg.getCannonPoint().getX(), cg.getCannonPoint().getY());

            ParallelAction shootingAction = new ParallelAction();
            float duration = cg.euclDist(cg.getCannonPoint(), p) / velocity;
            shootingAction.addAction(
                Actions.moveTo(p.getX(), p.getY(), duration, Interpolation.linear
            ));
            RunnableAction ra =  new RunnableAction(){
                public void run(){
                    shotActor.setDrawable(new TextureRegionDrawable(new TextureRegion(pCraterTex)));
                }
            };
            SequenceAction scaleSequence = new SequenceAction();
            scaleSequence.addAction(Actions.scaleTo(scaleUp,scaleUp, duration/2f, Interpolation.linear));
            scaleSequence.addAction(Actions.scaleTo(1,1, duration/2f, Interpolation.linear));
            scaleSequence.addAction(ra);
            shootingAction.addAction(scaleSequence);
            shotActor.addAction(shootingAction);
            gameStage.addActor(shotActor);
        }

    }

    public void oppShotUpdates() {
        List<Point> oppshots = cg.collectOppShots();
        int scaleFrom = 4;
        for (Point shot : oppshots) {
            //create image at cannon position
            final Image shotActor = new Image(oppshotTex);
            shotActor.setWidth(cg.width_ratio);
            shotActor.setHeight(cg.height_ratio);
            Point p = cg.cannonSpaceToScreenSpace(shot);
            shotActor.setOrigin(Align.center);
            shotActor.setScale(scaleFrom, scaleFrom);
            shotActor.setPosition(p.getX(), p.getY());

            SequenceAction scaleSequence = new SequenceAction();
            RunnableAction ra =  new RunnableAction(){
                public void run(){
                    shotActor.setDrawable(new TextureRegionDrawable(new TextureRegion(oppCraterTex)));
                }
            };

            float duration = 1.35f;
            Action scaleAction = Actions.scaleTo(1,1, duration, Interpolation.linear);
            scaleSequence.addAction(scaleAction);
            scaleSequence.addAction(ra);
            shotActor.addAction((scaleSequence));
            gameStage.addActor(shotActor);
        }
    }

    public void addBackgroundGuide(){
        Texture texture = mgr.get("grassland1680x945.png", Texture.class);
        TextureRegion textureRegion = new TextureRegion(texture);
        Image background = new Image(textureRegion);
        background.setSize(game.width, game.height);
        background.setPosition(0,0);

        background.addListener(new ActorGestureListener(){
            @Override
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                super.pan(event, x, y, deltaX, deltaY);
                camera.position.x -= (deltaX*Gdx.graphics.getDensity());
                camera.position.y -= (deltaY*Gdx.graphics.getDensity());

            }
        });

        gameStage.addActor(background);
    }

    public void addPrepStage() {
        final CountDownLatch latch = client.streamingPlay(cg);

        addBackgroundGuide();
        int row_height = game.width / 15;
        int col_width = game.width / 15;

        final Label title = new Label("ARE YOU READY?", CannonGdx.skin,"default");
        title.setSize(game.width, row_height*2);
        title.setPosition(0,game.height-row_height*2);
        title.setAlignment(Align.center);

        final TextButton ready = new TextButton("Ready Button", CannonGdx.skin,"default");
        ready.setSize(col_width*4,row_height);
        ready.setPosition(game.width-col_width*10,game.height-row_height*3);
        ready.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                ready.remove();
                title.remove();
                createGameUI();
                client.submitRequest(client.newReadyPlayRequest(true));
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        UIStage.addActor(title);
        UIStage.addActor(ready);
    }

    public void zoomSlider() {
        zoomSlider = new Slider(0.25f,2,0.05f,true, CannonGdx.skin);
        zoomSlider.setAnimateInterpolation(Interpolation.smooth);
        zoomSlider.setHeight(game.height*3/5f);
        zoomSlider.setPosition(game.width/20f,game.height*4/16f);
        zoomSlider.setValue(1f);
        zoomSlider.addListener(new InputListener(){
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                camera.zoom = zoomSlider.getValue();
                camera.update();
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                camera.zoom = zoomSlider.getValue();
                camera.update();
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
    }

    public void addZoomSlider(){
        UIStage.addActor(zoomSlider);
    }
    private void rotSlider(){
        rotSlider(0f, 360f);
    }
    private void rotSlider(float min, float max){
        rotSliderLabel = new Label("Rot: "+cg.rotation, CannonGdx.skin, "default");
        rotSliderLabel.setSize(game.width/4f, game.height/12f);
        rotSliderLabel.setPosition(0+game.width/16f,game.height/36f);
        rotSliderLabel.setAlignment(Align.center);

        rotSlider = new Slider(min, max,0.25f,false, CannonGdx.skin);
        rotSlider.setAnimateInterpolation(Interpolation.smooth);

        rotSlider.setWidth(game.width/4f);
        rotSlider.setPosition(0+game.width/16f,game.height/12f);
        rotSlider.setValue(cg.rotation);
        rotSlider.addListener(new InputListener(){
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                cg.rotation = rotSlider.getValue();
                rotSliderLabel.setText("Rot: "+cg.rotation);
                cg.setCannonRot(cg.rotation);
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                cg.rotation = rotSlider.getValue();
                rotSliderLabel.setText("Rot: "+cg.rotation);
                cg.setCannonRot(cg.rotation);
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
    }

    public void addRotSlider(){
        UIStage.addActor(rotSliderLabel);
        UIStage.addActor(rotSlider);
    }


    public void powerSlider() {
        powerSliderLabel = new Label("Pow: " + cg.power, CannonGdx.skin, "default");
        powerSliderLabel.setSize(game.width / 4f, game.height / 12f);
        powerSliderLabel.setPosition(game.width / 4f + game.width * 2 / 16f, game.height / 36f);
        powerSliderLabel.setAlignment(Align.center);

        powerSlider = new Slider(5, 35, 0.25f, false, CannonGdx.skin);
        powerSlider.setAnimateInterpolation(Interpolation.smooth);

        powerSlider.setWidth(game.width / 4f);
        powerSlider.setPosition(game.width / 4f + game.width * 2 / 16f, game.height / 12f);
        powerSlider.setValue(cg.power);
        powerSlider.addListener(new InputListener() {
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                cg.power = powerSlider.getValue();
                powerSliderLabel.setText("Pow: " + cg.power);
                //Gdx.app.log("touchDragged","slider Value:"+slider.getValue());
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                Gdx.app.log("up","slider Value:"+slider.getValue());
                cg.power = powerSlider.getValue();
                powerSliderLabel.setText("Pow: " + cg.power);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                Gdx.app.log("down","slider Value:"+slider.getValue());
                return true;
            }
        });
    }

    public void addPowerSlider(){
        UIStage.addActor(powerSliderLabel);
        UIStage.addActor(powerSlider);
    }

    public void pitchSlider() {
        pitchSliderLabel = new Label("Pitch: " + cg.pitch, CannonGdx.skin, "default");
        pitchSliderLabel.setSize(game.width / 4f, game.height / 12f);
        pitchSliderLabel.setPosition(game.width * 2 / 4f + game.width * 3 / 16f, game.height / 36f);
        pitchSliderLabel.setAlignment(Align.center);

        pitchSlider = new Slider(45, 85, 0.25f, false, CannonGdx.skin);
        pitchSlider.setAnimateInterpolation(Interpolation.smooth);
        pitchSlider.setWidth(game.width / 4f);
        pitchSlider.setPosition(game.width * 2 / 4f + game.width * 3 / 16f, game.height / 12f);
        pitchSlider.setValue(cg.pitch);
        pitchSlider.addListener(new InputListener() {
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                cg.pitch = pitchSlider.getValue();
                pitchSliderLabel.setText("Pitch: " + cg.pitch);
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                cg.pitch = pitchSlider.getValue();
                pitchSliderLabel.setText("Pitch: " + cg.pitch);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
    }

    public void addPitchSlider(){
        UIStage.addActor(pitchSliderLabel);
        UIStage.addActor(pitchSlider);
    }

    public void fireButton() {
        int row_height = game.width / 15;
        int col_width = game.width / 15;

        fireButton = new TextButton("Fire!", CannonGdx.skin);
        fireButton.setSize(col_width*4, row_height);
        fireButton.setPosition(game.width/2f-col_width*2,game.height*22/24);
        fireButton.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                client.submitRequest(client.newFirePlayRequest(cg.rotation, cg.power, cg.pitch));
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
    }

    public void addFireButton() {
        UIStage.addActor(fireButton);
    }

    public void setRotSliderRange(float minRot, float maxRot) {
        rotSlider(minRot, maxRot);
    }

    public void createGameUI() {
        zoomSlider();
        rotSlider();
        powerSlider();
        pitchSlider();
        fireButton();

    }

    public void addGameUI() {
        addZoomSlider();
        addRotSlider();
        addPowerSlider();
        addPitchSlider();
        addFireButton();
    }

    @Override
    public void resize(int width, int height) {
        UIStage.getViewport().update(width, height, true);
        gameStage.getViewport().update(width, height, false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        UIStage.dispose();
        gameStage.dispose();
    }

}
