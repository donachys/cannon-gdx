package com.donachys.cannongdx;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.17.1)",
    comments = "Source: cannongo.proto")
public final class CannonGoGrpc {

  private CannonGoGrpc() {}

  public static final String SERVICE_NAME = "cannongo.CannonGo";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.donachys.cannongdx.JoinRequest,
      com.donachys.cannongdx.JoinResponse> getJoinMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Join",
      requestType = com.donachys.cannongdx.JoinRequest.class,
      responseType = com.donachys.cannongdx.JoinResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.donachys.cannongdx.JoinRequest,
      com.donachys.cannongdx.JoinResponse> getJoinMethod() {
    io.grpc.MethodDescriptor<com.donachys.cannongdx.JoinRequest, com.donachys.cannongdx.JoinResponse> getJoinMethod;
    if ((getJoinMethod = CannonGoGrpc.getJoinMethod) == null) {
      synchronized (CannonGoGrpc.class) {
        if ((getJoinMethod = CannonGoGrpc.getJoinMethod) == null) {
          CannonGoGrpc.getJoinMethod = getJoinMethod = 
              io.grpc.MethodDescriptor.<com.donachys.cannongdx.JoinRequest, com.donachys.cannongdx.JoinResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "cannongo.CannonGo", "Join"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.donachys.cannongdx.JoinRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.donachys.cannongdx.JoinResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new CannonGoMethodDescriptorSupplier("Join"))
                  .build();
          }
        }
     }
     return getJoinMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.donachys.cannongdx.StreamingPlayRequest,
      com.donachys.cannongdx.StreamingPlayResponse> getStreamingPlayMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StreamingPlay",
      requestType = com.donachys.cannongdx.StreamingPlayRequest.class,
      responseType = com.donachys.cannongdx.StreamingPlayResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<com.donachys.cannongdx.StreamingPlayRequest,
      com.donachys.cannongdx.StreamingPlayResponse> getStreamingPlayMethod() {
    io.grpc.MethodDescriptor<com.donachys.cannongdx.StreamingPlayRequest, com.donachys.cannongdx.StreamingPlayResponse> getStreamingPlayMethod;
    if ((getStreamingPlayMethod = CannonGoGrpc.getStreamingPlayMethod) == null) {
      synchronized (CannonGoGrpc.class) {
        if ((getStreamingPlayMethod = CannonGoGrpc.getStreamingPlayMethod) == null) {
          CannonGoGrpc.getStreamingPlayMethod = getStreamingPlayMethod = 
              io.grpc.MethodDescriptor.<com.donachys.cannongdx.StreamingPlayRequest, com.donachys.cannongdx.StreamingPlayResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "cannongo.CannonGo", "StreamingPlay"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.donachys.cannongdx.StreamingPlayRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.donachys.cannongdx.StreamingPlayResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new CannonGoMethodDescriptorSupplier("StreamingPlay"))
                  .build();
          }
        }
     }
     return getStreamingPlayMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CannonGoStub newStub(io.grpc.Channel channel) {
    return new CannonGoStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CannonGoBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CannonGoBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CannonGoFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CannonGoFutureStub(channel);
  }

  /**
   */
  public static abstract class CannonGoImplBase implements io.grpc.BindableService {

    /**
     */
    public void join(com.donachys.cannongdx.JoinRequest request,
        io.grpc.stub.StreamObserver<com.donachys.cannongdx.JoinResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getJoinMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.donachys.cannongdx.StreamingPlayRequest> streamingPlay(
        io.grpc.stub.StreamObserver<com.donachys.cannongdx.StreamingPlayResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getStreamingPlayMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getJoinMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.donachys.cannongdx.JoinRequest,
                com.donachys.cannongdx.JoinResponse>(
                  this, METHODID_JOIN)))
          .addMethod(
            getStreamingPlayMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                com.donachys.cannongdx.StreamingPlayRequest,
                com.donachys.cannongdx.StreamingPlayResponse>(
                  this, METHODID_STREAMING_PLAY)))
          .build();
    }
  }

  /**
   */
  public static final class CannonGoStub extends io.grpc.stub.AbstractStub<CannonGoStub> {
    private CannonGoStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CannonGoStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CannonGoStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CannonGoStub(channel, callOptions);
    }

    /**
     */
    public void join(com.donachys.cannongdx.JoinRequest request,
        io.grpc.stub.StreamObserver<com.donachys.cannongdx.JoinResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getJoinMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.donachys.cannongdx.StreamingPlayRequest> streamingPlay(
        io.grpc.stub.StreamObserver<com.donachys.cannongdx.StreamingPlayResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getStreamingPlayMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class CannonGoBlockingStub extends io.grpc.stub.AbstractStub<CannonGoBlockingStub> {
    private CannonGoBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CannonGoBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CannonGoBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CannonGoBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.donachys.cannongdx.JoinResponse join(com.donachys.cannongdx.JoinRequest request) {
      return blockingUnaryCall(
          getChannel(), getJoinMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CannonGoFutureStub extends io.grpc.stub.AbstractStub<CannonGoFutureStub> {
    private CannonGoFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CannonGoFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CannonGoFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CannonGoFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.donachys.cannongdx.JoinResponse> join(
        com.donachys.cannongdx.JoinRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getJoinMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_JOIN = 0;
  private static final int METHODID_STREAMING_PLAY = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CannonGoImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CannonGoImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_JOIN:
          serviceImpl.join((com.donachys.cannongdx.JoinRequest) request,
              (io.grpc.stub.StreamObserver<com.donachys.cannongdx.JoinResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_STREAMING_PLAY:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.streamingPlay(
              (io.grpc.stub.StreamObserver<com.donachys.cannongdx.StreamingPlayResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CannonGoBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CannonGoBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.donachys.cannongdx.CannonGrpcGdx.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CannonGo");
    }
  }

  private static final class CannonGoFileDescriptorSupplier
      extends CannonGoBaseDescriptorSupplier {
    CannonGoFileDescriptorSupplier() {}
  }

  private static final class CannonGoMethodDescriptorSupplier
      extends CannonGoBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CannonGoMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CannonGoGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CannonGoFileDescriptorSupplier())
              .addMethod(getJoinMethod())
              .addMethod(getStreamingPlayMethod())
              .build();
        }
      }
    }
    return result;
  }
}
